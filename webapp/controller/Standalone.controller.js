sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast"
], function(Controller,JSONModel,Filter,FilterOperator,MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.Standalone", {

		onInit: function() {
			var ctrl = this;

			ctrl.getView().setModel(new JSONModel("model/departments.json"), "dept");
			ctrl.getView().setModel(new JSONModel("model/members.json"), "user");
			ctrl.getView().setModel(new JSONModel("model/leavetype.json"), "leaveTypes");
			ctrl.getView().setModel(new JSONModel("model/leave.json"), "leave");
			
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("FORM", "FORM_SUBMIT", this.onSumit, this);
		},
		onSubmitPress: function(oEvent) {
			var ctrl = this;
			var sApplicantDept = ctrl.getView().byId("applicantDept").getSelectedKey();
			var sApplcant = ctrl.getView().byId("ApplcantID").getSelectedKey();
			var sFillerID = ctrl.getView().byId("fillerID").getSelectedKey();
			var sLeaveType = ctrl.getView().byId("leaveType").getSelectedKey();
			var sFlexitime = ctrl.getView().byId("flexitime").getSelected();
			var sErrorMessage = "";

			if (sApplicantDept == "") sErrorMessage += "請填寫申請人部門.";
			if (sErrorMessage != "") {
				MessageToast.show(JSON.stringify(sErrorMessage));
			} else {
				var oModel = ctrl.getView().getModel("leave");

				oModel.setProperty("/applicantDept", sApplicantDept);
				oModel.setProperty("/Applcant", sApplcant);
				oModel.setProperty("/fillerID", sFillerID);
				oModel.setProperty("/leaveType", sLeaveType);
				oModel.setProperty("/flexitime", sFlexitime);

				//sap.ui.getCore().getEventBus().publish("FORM", "FORM_SUBMIT", oModel.oData);
				MessageToast.show(JSON.stringify(oModel.oData));
			}
		},
		changeDepartment: function(oEvent) {
			var ctrl = this;
			var sDepartmentID = oEvent.getParameters().selectedItem.getKey();
			var aFilter = [];

			// build filter array
			aFilter.push(new Filter("departmentId", FilterOperator.Contains, sDepartmentID));

			// filter binding
			var oList = ctrl.getView().byId("ApplcantID");
			var oBinding = oList.getBinding("items");
			oBinding.filter(aFilter);
		}
	
	});

});