sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Form.controller.DemoMenu", {

		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		},

		showEmbedded: function() {
			this.oRouter.navTo("Embedded");
		},

	});

});